import { useState } from 'react';

import { Box, Typography, Avatar } from '@mui/material';
import { SystemStyleObject, Theme } from '@mui/system';

import { Card } from '@components/atoms';
import { IContact } from 'react-coding-interview-shared/models';

export interface IContactCardProps {
  person: IContact;
  sx?: SystemStyleObject<Theme>;
}

function InlineInput({ text }: { text: any }) {
  const [inputText, setInputText] = useState(text);

  //consider debounce function after edit
  //consider style based on textStyle prop

  const onChangeHandler = (e: any) => {
    setInputText(e.target.value);
  };

  return <input onChange={onChangeHandler} value={inputText} type="text" />;
}

export const ContactCard: React.FC<IContactCardProps> = ({
  person: { name, email },
  sx,
}) => {
  return (
    <Card sx={sx}>
      <Box display="flex" flexDirection="column" alignItems="center">
        <Avatar />
        <Box textAlign="center" mt={2}>
          {/* <Typography variant="subtitle1" lineHeight="1rem"> */}

          {/* <InlineInput text={name} textStyle="title" /> */}
          <InlineInput text={name} />
          {/* </Typography> */}

          {/* <Typography variant="caption" color="text.secondary">
            {email}
          </Typography> */}

          <InlineInput text={email} />
        </Box>
      </Box>
    </Card>
  );
};
